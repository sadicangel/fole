﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fole.Controls
{
    /// <summary>
    /// Interaction logic for CropControl.xaml. Displays an image with scroll bars.
    /// </summary>
    public partial class CropControl : UserControl
    {
        /// <summary>
        /// The image property.
        /// </summary>
        public static readonly DependencyProperty ImageProperty = DependencyProperty.Register(nameof(Image), typeof(ImageSource), typeof(CropControl));
        /// <summary>
        /// The width size property.
        /// </summary>
        public static readonly DependencyProperty WidthSizeProperty = DependencyProperty.Register(nameof(WidthSize), typeof(double), typeof(CropControl));
        /// <summary>
        /// The height size property.
        /// </summary>
        public static readonly DependencyProperty HeightSizeProperty = DependencyProperty.Register(nameof(HeightSize), typeof(double), typeof(CropControl));
        /// <summary>
        /// The margin size property.
        /// </summary>
        public static readonly DependencyProperty MarginSizeProperty = DependencyProperty.Register(nameof(MarginSize), typeof(Thickness), typeof(CropControl));

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        public ImageSource Image { get => (ImageSource)GetValue(ImageProperty); set => SetValue(ImageProperty, value); }
        /// <summary>
        /// Gets or sets the size of the width.
        /// </summary>
        public double WidthSize { get => (double)GetValue(WidthSizeProperty); set => SetValue(WidthSizeProperty, value); }
        /// <summary>
        /// Gets or sets the size of the height.
        /// </summary>
        public double HeightSize { get => (double)GetValue(HeightSizeProperty); set => SetValue(HeightSizeProperty, value); }
        /// <summary>
        /// Gets or sets the size of the margin.
        /// </summary>
        public Thickness MarginSize { get => (Thickness)GetValue(MarginSizeProperty); set => SetValue(MarginSizeProperty, value); }

        /// <summary>
        /// Initializes a new instance of the <see cref="CropControl"/> class.
        /// </summary>
        public CropControl()
        {
            InitializeComponent();
            root.DataContext = this;
        }
    }
}
