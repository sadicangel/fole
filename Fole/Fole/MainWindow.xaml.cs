﻿using Fole.Source;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fole
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Gets or sets the ImageCrop worker.
        /// </summary>
        public ImageCrop Crop { get; set; } = new ImageCrop();

        /// <summary>
        /// Gets the open command.
        /// </summary>
        public ICommand OpenCommand { get; } = new RelayCommand<MainWindow>(w => SetSource(w));

        /// <summary>
        /// Gets the process command.
        /// </summary>
        public ICommand ProcessCommand { get; } = new RelayCommand<MainWindow>(w => Process(w));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            imageRoot.DataContext = Crop;
            menuRoot.DataContext = this;
        }

        /// <summary>
        /// Sets the source image on the current image crop worker.
        /// </summary>
        /// <param name="window">The window.</param>
        private static void SetSource(MainWindow window)
        {
            OpenFileDialog dialog = new OpenFileDialog()
            {
                Title = "Chose image.",
                Filter = "png | *.png"
            };
            if(dialog.ShowDialog(window).GetValueOrDefault())
                window.Crop.SetSource(dialog.FileName);
        }

        /// <summary>
        /// Starts the processing of the source image using the current image crop worker.
        /// </summary>
        /// <param name="window">The window.</param>
        private static void Process(MainWindow window)
        {
            try
            {
                window.Crop.Process();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
