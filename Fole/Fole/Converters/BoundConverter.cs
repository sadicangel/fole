﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Fole.Converters
{
    /// <summary>
    /// Converts the bounds of a window to values that take into consideration space required for scroll bars.
    /// </summary>
    /// <seealso cref="Fole.Converters.MarkupBaseConverter{Fole.Converters.BoundConverter}" />
    /// <seealso cref="System.Windows.Data.IValueConverter" />
    public sealed class BoundConverter : MarkupBaseConverter<BoundConverter>, IValueConverter
    {
        public double Width { get => (double)Application.Current.Resources[SystemParameters.VerticalScrollBarWidthKey]; }
        public double Height { get => 40; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)parameter == "Width" ? ((double)value - Width) / 2 : ((double)value) - Height;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
