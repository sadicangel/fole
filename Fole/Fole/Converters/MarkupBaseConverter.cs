﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Fole.Converters
{
    /// <summary>
    /// Serves as a base class for all converters by implementing <see cref="MarkupExtension"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="System.Windows.Markup.MarkupExtension" />
    public abstract class MarkupBaseConverter<T> : MarkupExtension where T : new()
    {
        /// <summary>
        /// Holds or creates an instance of the converter.
        /// </summary>
        private static Lazy<T> Instance = new Lazy<T>(() => new T());

        /// <summary>
        /// When implemented in a derived class, returns an object that is provided as the value of the target property for this markup extension.
        /// </summary>
        /// <param name="serviceProvider">A service provider helper that can provide services for the markup extension.</param>
        /// <returns></returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Instance.Value;
        }
    }
}
