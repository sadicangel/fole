﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Fole.Source
{
    /// <summary>
    /// Serves as a base for all classes that require INotifyPropertyChanged.
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class NotifyBase : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sets the specified field and notifies that its property changed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field">The field to set.</param>
        /// <param name="value">The value to set on the field.</param>
        /// <param name="name">The property name.</param>
        protected virtual void Set<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            Set(ref field, value, null, name);
        }

        /// <summary>
        /// Sets the specified field and notifies that its property changed.
        /// Also notifies change in all properties on the given array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field">The field to set.</param>
        /// <param name="value">The value to set on the field.</param>
        /// <param name="properties">The properties to notify change.</param>
        /// <param name="name">The property name.</param>
        protected void Set<T>(ref T field, T value, string[] properties, [CallerMemberName] string name = null)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                NotifyPropertyChanged(name);
                if(properties != null)
                    foreach (string property in properties)
                        NotifyPropertyChanged(property);
            }
        }

        /// <summary>
        /// Gets variable arguments as an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        protected T[] GetArgs<T>(params T[] args)
        {
            return args;
        }

        /// <summary>
        /// Notifies that a property changed.
        /// </summary>
        /// <param name="name">The name.</param>
        protected void NotifyPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
