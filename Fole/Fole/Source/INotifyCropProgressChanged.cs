﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fole.Source
{
    public delegate void CropProgressChangedEventHandler(object sender, CropProgressChangedEventArgs sampleParam);

    public interface INotifyCropProgressChanged
    {
        event CropProgressChangedEventHandler CropProgressChanged;
    }

    public sealed class CropProgressChangedEventArgs : EventArgs
    {
        public double Total { get; }
        public double Value { get; }

        public CropProgressChangedEventArgs(double total, double value)
        {
            Total = total;
            Value = value;
        }
    }
}
