﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace Fole.Source
{
    /// <summary>
    /// Image crop worker.
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public sealed class ImageCrop : NotifyBase, INotifyCropProgressChanged
    {
        /// <summary>
        /// The source image.
        /// </summary>
        private Bitmap _source;
        /// <summary>
        /// The result image.
        /// </summary>
        private Bitmap _result;

        /// <summary>
        /// Gets or sets the source image.
        /// </summary>
        public Bitmap Source { get => _source; private set => Set(ref _source, value, GetArgs(nameof(SourceBinding))); }

        /// <summary>
        /// Gets the source image to use as binding.
        /// </summary>
        public object SourceBinding { get => Source != null ? BitmapToBitmapSource(Source) : Binding.DoNothing; }

        /// <summary>
        /// Gets or sets the result image.
        /// </summary>
        public Bitmap Result { get => _result; private set => Set(ref _result, value, GetArgs(nameof(ResultBinding))); }

        /// <summary>
        /// Gets the result image to use as binding.
        /// </summary>
        public object ResultBinding { get => Result != null ? BitmapToBitmapSource(Result) : Binding.DoNothing; }

        /// <summary>
        /// Get or sets the Pen to draw cut lines.
        /// </summary>
        public Pen Pen { get; set; } = new Pen(new SolidBrush(Color.Black));

        /// <summary>
        /// Gets or sets the horizontal starting point where the process will start.
        /// </summary>
        public int X { get => _x; set => Set(ref _x, value); }
        private int _x;

        /// <summary>
        /// Gets or sets the height of the first horizontal line..
        /// </summary>
        public int Y1 { get => _y1; set => Set(ref _y1, value); }
        private int _y1;

        /// <summary>
        /// Gets or sets the height of the second horizontal line..
        /// </summary>
        public int Y2 { get => _y2; set => Set(ref _y2, value); }
        private int _y2;

        /// <summary>
        /// Gets or sets the number of times to cut the image.
        /// </summary>
        public int Times { get => _times; set => Set(ref _times, value); }
        private int _times;

        /// <summary>
        /// Occurs when progress changes.
        /// </summary>
        public event CropProgressChangedEventHandler CropProgressChanged;

        /// <summary>
        /// Sets the source image.
        /// </summary>
        /// <param name="img"></param>
        public void SetSource(string img)
        {
            if (!String.IsNullOrWhiteSpace(img))
            {
                Source = Image.FromFile(img) as Bitmap;
                Result = null;
            }
        }

        /// <summary>
        /// Processes the source image.
        /// </summary>
        public void Process()
        {
            const int width = 70;
            const int size = 20;
            using (Bitmap bmp = new Bitmap(width * Times, Source.Height, PixelFormat.Format24bppRgb))
            {
                for (int i = 1; i <= Times; ++i)
                {
                    using (Graphics gfx = Graphics.FromImage(bmp))
                    {
                        bmp.SetResolution(80, 60);
                        gfx.SmoothingMode = SmoothingMode.AntiAlias;
                        gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        Rectangle rect = new Rectangle((i - 1) * width, 0, i * width, Source.Height);
                        gfx.DrawImage(Source, rect, X + i * size, 0, i * width, Source.Height, GraphicsUnit.Pixel);
                        gfx.DrawRectangle(Pen, rect);
                    }
                    NotifyProgressChanged(Times, i);
                }
                using (Graphics gfx = Graphics.FromImage(bmp))
                {
                    gfx.DrawLine(Pen, new Point(0, Y1), new Point(bmp.Width, Y1));
                    gfx.DrawLine(Pen, new Point(0, Y2), new Point(bmp.Width, Y2));
                }
                Result = (Bitmap)bmp.Clone();
            }
        }

        /// <summary>
        /// Notifies that progress changed.
        /// </summary>
        /// <param name="total">The total.</param>
        /// <param name="value">The value.</param>
        private void NotifyProgressChanged(double total, double value)
        {
            Debug.WriteLine($"{value}/{total}");
            CropProgressChanged?.Invoke(this, new CropProgressChangedEventArgs(total, value));
        }

        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        /// <summary> 
        /// Converts a <see cref="Bitmap"/> into a WPF <see cref="BitmapSource"/>. 
        /// </summary> 
        /// <remarks>
        /// Uses GDI to do the conversion. Hence the call to the marshalled DeleteObject.
        /// </remarks> 
        /// <param name="source">The source bitmap.</param> 
        /// <returns>A BitmapSource</returns> 
        private BitmapSource BitmapToBitmapSource(Bitmap source)
        {
            var hBitmap = source.GetHbitmap();
            var result = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(hBitmap);

            return result;
        }


    }
}
